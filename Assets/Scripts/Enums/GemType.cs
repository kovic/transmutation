﻿/// <summary>
/// Gem types
/// </summary>
public enum GemType
{
    None,
    Alpha,
    Beta,
    Gamma
}
