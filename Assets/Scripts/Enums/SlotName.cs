﻿/// <summary>
/// Slot names
/// </summary>
public enum SlotName
{
    None,
    X,
    Y,
    Z,
    W
}
