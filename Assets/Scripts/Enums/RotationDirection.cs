﻿/// <summary>
/// Rotation direction types
/// </summary>
public enum RotationDirection 
{
    None,
    Clockwise,
    CounterClockwise
}
