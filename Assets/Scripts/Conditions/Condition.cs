﻿using System.Collections.Generic;

/// <summary>
/// Class that holds condition properties that can be declared in the editor.
/// </summary>
[System.Serializable]
public class Condition
{
    public RotationDirection RotationDirection;
    public int RotationNumber;
    public bool HasGemCondition;
    public GemInfo GemInfo;
    public List<SlotName> ConditionSlots;     
}
