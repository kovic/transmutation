﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that overlooks if all the win conditions are met.
/// </summary>
public class GameManager : MonoBehaviour 
{
    // Singleton field
    public static GameManager Instance = null;

    // Reference to UI object that handles rotation win condition display
    [SerializeField] private RotationCounter RotationCounter;
    
    private void Awake()
    {
        // Singleton instantiation
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject);
    }

    private void Start()
    {
        // Subscrition to event count wheel rotation
        SlotManager.Instance.WinConditionEvent += InitializeWinTimer;
        SlotManager.Instance.ExitWinConditionEvent += CloseWinTimer;
        RotationCounter.GameWonEvent += GameWon;
    }
    
    /// <summary>
    /// Triggered when all win conditions are met. 
    /// </summary>
    public void GameWon()
    {
        // Notify MenuController to display the win screen
        MenuController.Instance.ShowGameWonScreen();
    }

    /// <summary>
    /// Triggered when all Gamma gems are set. 
    /// </summary>
    private void InitializeWinTimer()
    {
        // Notify RotationCounter to initialize the counter display
        RotationCounter.Initialize();
    }

    /// <summary>
    /// Triggered when when Gamma gem is picked up from the slot, if the win condition was met before
    /// </summary>
    private void CloseWinTimer()
    {
        // Notify RotationCounter to terminate the counter display
        RotationCounter.Terminate();
    }
}
