﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Class that manipulates the wheel rotations
/// </summary>
public class WheelController : MonoBehaviour, IDragHandler, IEndDragHandler
{
    #region PUBLIC FIELDS    
    // Singleton field
    public static WheelController Instance = null;
    public AudioClip RotationSoundClip;
    #endregion

    #region PRIVATE FIELDS
    private Transform _transform;    
    private Transform _indicator;
    private Camera _camera;
    private bool _isDraging;
    private float _currentRotation;
    private float _fullRotation = 360f;
    private float _cameraOffsetY;
    private float _rotation = 0;
    private Quaternion _defaultRotation;
    private Vector3 _lastPoint;
    #endregion

    #region AWAKE
    private void Awake()
    {
        // Initialize singleton
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject);
    }
    #endregion

    #region START
    private void Start()
    {
        InitialiteValues();        
    }
    #endregion
    
    #region EVENTS
    /// <summary>
    /// Notifies when the wheel has been fully rotated
    /// </summary>
    /// <param name="rotation">Rotation direction</param>
    public delegate void WheelRotationDelegate(RotationDirection rotation);
    public event WheelRotationDelegate RotationDetectionEvent; 

    /// <summary>
    /// Notifies when the wheel has started or stopped rotation
    /// </summary>
    public delegate void WheelDelegate();
    public event WheelDelegate WheelRotationStartEvent;
    public event WheelDelegate WheelRotationStopEvent;
    #endregion

    #region PRIVATE METHODS
    /// <summary>
    /// Set initial values
    /// </summary>
    private void InitialiteValues()
    {
        _transform = transform;
        _camera = Camera.main;

        _defaultRotation = _transform.rotation;
        _cameraOffsetY = _camera.transform.position.y - _transform.position.y;
        _currentRotation = 0f;
        _lastPoint = _transform.TransformDirection(Vector3.forward);
        _lastPoint.y = 0;
    }

    /// <summary>
    /// Handles wheel rotation
    /// </summary>
    public void OnDrag(PointerEventData pointerEventData)
    {
        // When wheel starts rotating, calculate direction of the mouse relative to the wheel rotation and trigger the event
        if (!_isDraging)
        {
            _isDraging = true;
            CalculateInitialRotationRelation();
            WheelRotationStartEvent();
        }
        RotateWheel();
        CalculateRotation();
    }

    /// <summary>
    /// Sets wheel to its initial position and reset the values when wheel is released
    /// </summary>
    public void OnEndDrag(PointerEventData pointerEventData)
    {
        if (_isDraging)
        {
            _transform.rotation = _defaultRotation;
            ResetValues();
        }
    }

    /// <summary>
    /// Returns mouse world position
    /// </summary>
    /// <returns>Mouse world position</returns>
    private Vector3 GetMouseWorldPosition()
    {
        //Get mouse position
        Vector3 mousePosition = Input.mousePosition;

        //Adjust mouse z position
        mousePosition.z = _cameraOffsetY;
        
        return _camera.ScreenToWorldPoint(mousePosition);
    }

    /// <summary>
    /// Calculates direction of the mouse relative to the original wheel rotation
    /// </summary>
    private void CalculateInitialRotationRelation()
    {
        //Get a world position for the mouse
        Vector3 mouseWorldPosition = GetMouseWorldPosition();

        //Get the mouse position relative to the original rotation of the wheel
        _currentRotation = -Mathf.Atan2(_transform.position.z - mouseWorldPosition.z, _transform.position.x - mouseWorldPosition.x) * Mathf.Rad2Deg;
    }

    /// <summary>
    /// Rotates the wheel
    /// </summary>
    private void RotateWheel()
    {
        //Get a world position for the mouse
        Vector3 mouseWorldPosition = GetMouseWorldPosition(); 

        //Get the angle to rotate and rotate
        float angle = -Mathf.Atan2(_transform.position.z - mouseWorldPosition.z, _transform.position.x - mouseWorldPosition.x) * Mathf.Rad2Deg;

        //Subtracks the relative mouse direction from original wheel rotation
        angle -= _currentRotation;
        _transform.localRotation = Quaternion.Euler(0, angle, 0);        
    }

    /// <summary>
    /// Triggers clockwise rotation event
    /// </summary>
    private void ClockWiseRotationTrigger()
    {
        _rotation -= _fullRotation;
        if (RotationDetectionEvent != null)
            RotationDetectionEvent(RotationDirection.Clockwise); 
    }

    /// <summary>
    /// Triggers counter clockwise rotation event
    /// </summary>
    private void CounterClockWiseRotationTrigger()
    {
        _rotation += _fullRotation;
        if (RotationDetectionEvent != null)
            RotationDetectionEvent(RotationDirection.CounterClockwise); 
    }

    /// <summary>
    /// Calculate the rotation of the wheel
    /// </summary>
    private void CalculateRotation()
    {
        // Get current direction of the wheel
        Vector3 facing = _transform.TransformDirection(Vector3.forward);
        facing.y = 0;      

        // Calculate the angle between last position and the current position
        float angle = Vector3.Angle(_lastPoint, facing);

        // Determin if clockwise or counter clock wise rotation is made
        if (Vector3.Cross(_lastPoint, facing).y < 0)
            angle *= -1; 

        // Play clip if wheel is rotated
        if (angle != 0 && !SoundManager.Instance.IsSoundPlaying())
        {
            SoundManager.Instance.PlayLoopSoundClip(RotationSoundClip);
        }
        else
        {
            SoundManager.Instance.EndLoopingClip();
        }

        // Adjust values
        _rotation += angle;      
        _lastPoint = facing;

        // Trigger rotation event if full rotation has been made
        if (_rotation >= _fullRotation) 
            ClockWiseRotationTrigger();
        else if (_rotation <= -_fullRotation)
            CounterClockWiseRotationTrigger();
    }

    /// <summary>
    /// Reset values
    /// </summary>
    private void ResetValues()
    {
        _isDraging = false;
        _currentRotation = 0f;        
        _lastPoint = _transform.TransformDirection(Vector3.forward);
        _rotation = 0f;
        SoundManager.Instance.EndLoopingClip();

        if (WheelRotationStopEvent != null)
            WheelRotationStopEvent();
    }
    #endregion
}
