﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that evaluates if gem instantiation conditions are met and handles their instantiation
/// </summary>
public class GemInstantiationManager : MonoBehaviour 
{
    #region PUBLIC FIELDS
    // Singleton field
    public static GemInstantiationManager Instance = null;
    #endregion

    #region PRIVATE FIELDS
    // Gem prefab
    [SerializeField] private GameObject _gemObject;
    [SerializeField] private List<GemInfo> _gemInfoList;
    [SerializeField] private AudioClip _gemInstantiationClip;
    // Field that holds info about the direction of the current rotation
    private RotationDirection _currentRotationDirection = RotationDirection.None;
    #endregion

    #region EVENTS
    /// <summary>
    /// Gem instantiation event
    /// </summary>
    /// <param name="gem">Instantiated gem</param>
    public delegate void GemInstantiationDelegate(Gem gem);
    public event GemInstantiationDelegate GemInstantiationEvent;
    #endregion

    #region AWAKE
    private void Awake()
    {
        // Singleton instantiation
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject);
    }
    #endregion

    #region START
    private void Start()
    {
        // Initialize gem instatiation condition for each gem type
        foreach (GemInfo gemInfo in _gemInfoList)
            gemInfo.InitializeGemCondition();

        // Subscribe to wheel rotation events
        WheelController.Instance.RotationDetectionEvent += EvaluateGemConditions;
    }
    #endregion

    #region PRIVATE METHODS
    /// <summary>
    /// Instantiate the gem
    /// </summary>
    /// <param name="gemInfo">Gem information about the gem that is instantiated</param>
    private void InstantiateGem(GemInfo gemInfo)
    {
        // Get the the instantiation slot. Will be set to null if it's not empty.
        Transform instantiationTransform = SlotManager.Instance.GetGemInstantiationTransform(gemInfo); 
        
        if (instantiationTransform != null)
        {
            // Create gem and set initial the values
            GameObject newGemObject = Instantiate(_gemObject, instantiationTransform);
            newGemObject.GetComponent<MeshRenderer>().material = gemInfo.Material;
            Gem gem = newGemObject.GetComponent<Gem>();
            gem.GemInfo = gemInfo;

            // Trigger instantiation event
            GemInstantiationEvent(gem);

            // Play instantiation clip
            SoundManager.Instance.PlaySoundClip(_gemInstantiationClip);

            // Reset condition values
            foreach (GemInfo gemInf in _gemInfoList)
                gemInf.InitializeGemCondition();
            _currentRotationDirection = RotationDirection.None;
        }
        else
        {
            // Warning debug message if the slot if full
            Debug.LogWarning("Gem slot is full.Cannot create " + gemInfo.Type + "!");
        }
    }

    /// <summary>
    /// Evaluates gem instantiation conditions. Should be called after each full wheel rotation.
    /// </summary>
    private void EvaluateGemConditions(RotationDirection rotation)
    {
        // Save current rotation direction
        _currentRotationDirection = rotation;

        // Evaluate current condition for each gem type
        foreach (GemInfo gemInfo in _gemInfoList)
        {
            // Cache current condition
            Condition currentCondition = gemInfo.CurrentCondition;       
            bool conditionReached = false;

            // Check if any gems should be placed in certain slots
            if (currentCondition.HasGemCondition)
            {
                // Check if gems are placed accordingly
                bool gemConditionReached = CheckGemCondition(currentCondition);

                // If gem are not placed correctly, reset gem conditions and skip to the next iteration
                if (!gemConditionReached)
                {
                    Debug.Log("GEM CONDITION FOR " + gemInfo.Type + " FAILED! Conditions are reset!");
                    gemInfo.InitializeGemCondition();
                    continue;
                }
                Debug.Log("Gem condition for " + gemInfo.Type + " is reached");
            }

            // Check if wheel rotation direction if correct
            if (currentCondition.RotationDirection == _currentRotationDirection)
            {
                // Check if rotation count condition is met
                conditionReached = CheckRotationCondition(gemInfo, _currentRotationDirection);
                Debug.Log("Delta rotation for " + gemInfo.Type + " is: " + gemInfo.DeltaCountRotation);
            }
            else
            {
                // If whel rotation was incorrect, reset the current gem condition to the first one and reset counter values
                gemInfo.InitializeGemCondition();
            }

            // Check if current condition is reached
            if (conditionReached)
            {
                Debug.Log("Condition reached: " + gemInfo.Type);
                // Try to move to the next condition an if there isn't any, instantiate the gem
                bool canMoveToNextCondition = gemInfo.MoveToNextCondition();
                if (!canMoveToNextCondition)
                {
                    InstantiateGem(gemInfo);                    
                }
            }
        }        
    }

    /// <summary>
    /// Check if gem's rotation condition is met
    /// </summary>
    /// <param name="gemInfo">Gem information</param>
    /// <param name="rotationDirection">Current wheel rotation direction</param>
    /// <returns>Is delta rotation count equal to rotation number condition</returns>
    private bool CheckRotationCondition(GemInfo gemInfo, RotationDirection rotationDirection)
    {
        // Increment delta rotation for a given gem
        gemInfo.DeltaCountRotation++;
        return gemInfo.DeltaCountRotation == gemInfo.CurrentCondition.RotationNumber;
    }

    /// <summary>
    /// Check if gem's gem condition is met
    /// </summary>
    /// <param name="currentCondition">Information about the current gem instantiation condition</param>
    /// <returns>If necessary gems are placed in the correct slots</returns>
    private bool CheckGemCondition(Condition currentCondition)
    {
        // Number of condition slots to be filled with gems
        int numberSlotsToBeFilled = currentCondition.ConditionSlots.Count;
        // Temporary field holding the filled slots number
        int numberOfFilledSlots = 0;
        // For each condition slot, check if it is filled with the appropriate gem
        foreach (SlotName slot in currentCondition.ConditionSlots)
        {
            // Get type of gem that's in the slot and increment the counter field if it's the right one
            GemType type = SlotManager.Instance.GetGemInSlot(slot);
            if (type == currentCondition.GemInfo.Type)
            {
                numberOfFilledSlots++;
            }
        }
        return numberOfFilledSlots == numberSlotsToBeFilled;      
    }
    #endregion
}
