﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that manages all the slots
/// </summary>
public class SlotManager : MonoBehaviour 
{
    #region PUBLIC FIELDS
    // Singleton field
    public static SlotManager Instance = null;
    #endregion

    #region PRIVATE FIELDS
    // Field to hold slot in which the gem is to be instantiated
    private Slot _instantiationSlot;
    private Slot[] _slotArray;
    private Dictionary<Slot, Transform> _slotTransformsDict = new Dictionary<Slot, Transform>();
    private bool _winCondition = false;
    #endregion

    #region EVENT
    /// <summary>
    /// Notify when the win condition is met or when it's been exited
    /// </summary>
    public delegate void WinConditionDelegate();
    public event WinConditionDelegate WinConditionEvent;
    public event WinConditionDelegate ExitWinConditionEvent;
    #endregion

    #region AWAKE
    private void Awake()
    {
        // Singleton instantiation
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject);
    }
    #endregion

    #region
    private void Start()
    {
        // Put all slot components into the array and initialize the dictionary that holds slots and the respective transforms 
        _slotArray = GetComponentsInChildren<Slot>();
        foreach (Slot slot in _slotArray)
        {
            _slotTransformsDict.Add(slot, slot.transform);
        }

        // Subscribe to gem instantiation event
        GemInstantiationManager.Instance.GemInstantiationEvent += CreateGemOnSlot;
    }
    #endregion

    #region PUBLIC METHODS
    /// <summary>
    /// Returns the gem type that's in the given slot
    /// </summary>
    /// <param name="slotName">Name of the slot</param>
    /// <returns>Type of gem in the slot</returns>
    public GemType GetGemInSlot(SlotName slotName)
    {
        // Get gem information about the gem in the slot. If slot is empty, it is set to null.
        GemInfo gemInfo = GetSlot(slotName).GemInfo;
        if (gemInfo != null)
        {
            return gemInfo.Type;
        }
        else
        {
            return GemType.None;
        }
    }    

    /// <summary>
    /// Returns the transform of the slot in which a gem is to be instantiated.
    /// </summary>
    /// <param name="gemInfo">Gem information</param>
    /// <returns>Instantiation transform or null, of the slot is full</returns>
    public Transform GetGemInstantiationTransform(GemInfo gemInfo)
    {
        foreach (SlotName name in gemInfo.InstantiationSlots)
        {
            Slot slot = GetSlot(name);
            if (slot.IsEmpty)
            {
                // If slot is empty, save the slot in the appropriate field
                _instantiationSlot = slot;
                return _slotTransformsDict[slot];
            }
        }
        // Slot is full
        return null;
    }
    #endregion

    #region
    /// <summary>
    /// Returns the slot of the given name
    /// </summary>
    /// <param name="name">Name of the slot that is to be returned</param>
    /// <returns>Slot of the given name</returns>
    private Slot GetSlot(SlotName name)
    {
        foreach (Slot slot in _slotArray)
        {
            if (slot.SlotName == name)
                return slot;
        }
        Debug.LogWarning("Slot's name has not been assigned");
        return null; 
    }

    /// <summary>
    /// Sets the slot values when gem is instantiated in it
    /// </summary>
    /// <param name="gem">Instantiated gem</param>
    private void CreateGemOnSlot(Gem gem)
    {
        _instantiationSlot.OnGemCreated(gem);
        
        // Subscribe to pick up and drop events of the gem
        gem.GemPickUpEvent += OnGemPickUp;
        gem.GemDropEvent += OnGemDrop;
    }

    /// <summary>
    /// Notify slot that the gem was picked up and adjust win condition if necessary
    /// </summary>
    /// <param name="targetSlot">Slot name</param>
    /// <param name="gemInfo">Gem information</param>
    private void OnGemPickUp(SlotName targetSlot, GemInfo gemInfo)
    {
        Slot slot = GetSlot(targetSlot);
        slot.OnGemPickup(gemInfo);

        // If win condition was, notify that it was exited
        if (_winCondition)
        {
            ExitWinConditionEvent();
        }
    }

    /// <summary>
    /// Notify slot that the gem was dropped and check win condition
    /// </summary>
    /// <param name="targetSlot">Slot name</param>
    /// <param name="gemInfo">Gem information</param>
    private void OnGemDrop(SlotName targetSlot, GemInfo gemInfo) 
    {
        Slot slot = GetSlot(targetSlot);
        slot.OnGemDrop(gemInfo);        

        // Check win condition every time a gem is put on slot
        CheckWinCondition();
    }

    /// <summary>
    /// Check if win condition is met
    /// </summary>
    private void CheckWinCondition()
    {
        // Temporary field that holds the count of slots filled with Gamma gem
        int slotCounter = 0;
        // Go through every slot and see if it has a gem and check its type if ti does
        foreach (Slot slot in _slotArray)
        {
            if (slot.GemInfo == null)
            {
                return;
            }
            else if (slot.GemInfo.Type == GemType.Gamma)
            {
                slotCounter++;
            }
        }

        // Check if all slots have Gamma gem in them
        if (slotCounter == _slotArray.Length)
        {
            _winCondition = true;
            WinConditionEvent();
        }
    }
    #endregion
}
