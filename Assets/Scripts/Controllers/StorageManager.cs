﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageManager : MonoBehaviour 
{
    #region PUBLIC FIELDS   
    // Singleton field
    public static StorageManager Instance = null;
    #endregion

    #region AWAKE
    private void Awake()
    {
        // Singleton instantiation
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject); 
    }
    #endregion

    #region START
    private void Start()
    {
        // Subscribe to gem instantiation event
        GemInstantiationManager.Instance.GemInstantiationEvent += OnGemCreated;
    }
    #endregion

    #region PRIVATE METHODS
    /// <summary>
    /// Subscribes to gem pickup and drop events. Triggered when gem is instantiated.
    /// </summary>
    /// <param name="gem">Instantiated gem</param>
    private void OnGemCreated(Gem gem)
    {
        gem.GemStoragePickUpEvent += OnGemPickUp;
        gem.GemStorageDropEvent += OnGemDrop;
    }

    /// <summary>
    /// Notifies storage that gem is dropped on it
    /// </summary>
    /// <param name="gemTransform">Gem's transform</param>
    /// <param name="storage">Storage on which the gem is dropped</param>
    private void OnGemDrop(Transform gemTransform, Storage storage)
    {
        storage.OnGemDrop(gemTransform);
    }

    /// <summary>
    /// Notifies storage that gem is pick up from it
    /// </summary>
    /// <param name="storage">Storage from which the gem is picked up</param>
    private void OnGemPickUp(Storage storage)
    {
        storage.OnGemPickUp();
    }
    #endregion
}
