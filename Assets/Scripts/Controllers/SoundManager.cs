﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Manages all sound manipulation in the scene
/// </summary>
public class SoundManager : MonoBehaviour 
{
    #region PUBLIC FIELDS
    // Singleton field
    public static SoundManager Instance = null;
    #endregion

    #region PRIVATE FIELDS
    [SerializeField] private AudioSource MusicAudioSource;
    [SerializeField] private AudioSource SoundAudioSource;
    [SerializeField] private Slider MusicSlider;
    [SerializeField] private Slider SoundSlider;
    [SerializeField] private AudioClip TestClip;

    private string _music = "Music";
    private string _sound = "Sound";
    #endregion

    #region AWAKE
    private void Awake()
    {
        // Singleton instantiation
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject);
    }
    #endregion

    #region START
    private void Start()
    {
        GetVolumeValues();
        InitializeValues();
    }
    #endregion

    #region PUBLIC METHODS
    /// <summary>
    /// Plays sound clip
    /// </summary>
    /// <param name="clip">Clip to play</param>
    public void PlaySoundClip(AudioClip clip)
    {
        SoundAudioSource.clip = clip;
        SoundAudioSource.Play();
    }

    /// <summary>
    /// Plays sound clip in a loop
    /// </summary>
    /// <param name="clip">Clip to play</param>
    public void PlayLoopSoundClip(AudioClip clip)
    {
        SoundAudioSource.loop = true;
        SoundAudioSource.clip = clip;
        SoundAudioSource.Play();
    }

    /// <summary>
    /// Ends the loop if one existed
    /// </summary>
    public void EndLoopingClip()
    {
        if (SoundAudioSource.loop)
            SoundAudioSource.loop = false;
    }

    /// <summary>
    /// Checks if sound is currently playing
    /// </summary>
    /// <returns>If sound is playing</returns>
    public bool IsSoundPlaying()
    {
        return SoundAudioSource.isPlaying;
    }
    #endregion

    #region PRIVATE METHODS
    /// <summary>
    /// Adjust music volume and write it to player prefs
    /// </summary>
    /// <param name="value">Value of the volume</param>
    private void ToggleMusic(float value)
    {
        MusicAudioSource.volume = value;
        PlayerPrefs.SetFloat(_music, value);
    }

    /// <summary>
    /// Adjust sound volume and write it to player prefs
    /// </summary>
    /// <param name="value">Value of the volume</param>
    private void ToggleSound(float value)
    {
        SoundAudioSource.volume = value;
        PlayerPrefs.SetFloat(_sound, value);
        PlaySoundClip(TestClip);
    }

    /// <summary>
    /// Get volume values from the player prefs, if they exist
    /// </summary>
    private void GetVolumeValues()
    {
        if (PlayerPrefs.HasKey(_music))
        {
            MusicSlider.value = PlayerPrefs.GetFloat(_music);
        }
        if (PlayerPrefs.HasKey(_sound))
        {
            SoundSlider.value = PlayerPrefs.GetFloat(_sound);
        }
    }
   
    /// <summary>
    /// Set audio source volume values and add listeners to slider value change
    /// </summary>
    private void InitializeValues()
    {
        MusicAudioSource.volume = MusicSlider.value;
        SoundAudioSource.volume = SoundSlider.value;
        MusicSlider.onValueChanged.AddListener(ToggleMusic);
        SoundSlider.onValueChanged.AddListener(ToggleSound);
    }
    #endregion
}
