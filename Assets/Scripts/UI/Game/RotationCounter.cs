﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class that handles rotation counter display when gem win condition is met
/// </summary>
public class RotationCounter : MonoBehaviour 
{   
    #region PRIVATE FIELDS
    [SerializeField] private GameObject CounterDisplay;
    [SerializeField] private Text TimerText;
    [SerializeField] private Text CounterText;
    [SerializeField] private int _goalRotations;
    [SerializeField] private float _initialTimerValue;

    private int _counter = 0;
    private float _timer;
    private bool _timerStarted = false;
    private bool _initialized = false;
    private string _timerPrefix = "Time: ";
    private string _counterPrefix = "Count: ";
    #endregion

    #region EVENTS
    /// <summary>
    /// Event that is triggered when game win condition is met
    /// </summary>
    public delegate void GameWonDelegate();
    public event GameWonDelegate GameWonEvent;
    #endregion

    private void Start()
    {
        // Hide counter display on initialization
        CounterDisplay.SetActive(false);

        // Subscribe to wheel rotation events
        WheelController.Instance.WheelRotationStartEvent += StartTimer;
        WheelController.Instance.WheelRotationStopEvent += StopTimer;
        WheelController.Instance.RotationDetectionEvent += CountWheelRotations;
    }

    private void Update()
    {
        // If timer is started, handle rotation count and timer count down
        if (_timerStarted)
        {
            CountDown();
            if (CheckWheelCounter())
            {
                GameWonEvent();
                StopTimer();
            }
        }
    }

    /// <summary>
    /// Initialize values for rotation counter
    /// </summary>
    public void Initialize()
    {
        _initialized = true;
        ResetValues();
        TimerText.text = _timerPrefix + _timer.ToString("F");
        CounterText.text = _counterPrefix + _counter.ToString();
        CounterDisplay.SetActive(true);
    }

    /// <summary>
    /// Close rotation counter and reset its values
    /// </summary>
    public void Terminate()
    {
        _initialized = false;
        _timerStarted = false;
        CounterDisplay.SetActive(false);
        ResetValues();
    }

    /// <summary>
    /// Count wheel rotation if timer is started
    /// </summary>
    /// <param name="direction">Parameter is not used but is necessary as an event parameter</param>
    private void CountWheelRotations(RotationDirection direction)
    {
        if (_timerStarted)
            _counter++;
    }

    /// <summary>
    /// Display the current rotation count and check if win condition is met
    /// </summary>
    /// <returns>Is current number of wheel rotations equal to number of rotations set as win condition</returns>
    private bool CheckWheelCounter()
    {
        CounterText.text = _counterPrefix + _counter.ToString();
        return _counter == _goalRotations;
    }

    /// <summary>
    /// Count down the timer
    /// </summary>
    private void CountDown()
    {
        if (_timer > 0)
        {
            _timer -= Time.deltaTime;
        }
        else
        {
            _timer = 0f;
            StopTimer();
        }
        TimerText.text = _timerPrefix + _timer.ToString("F");
    }

    /// <summary>
    /// Triggered when wheel starts rotating. 
    /// </summary>
    private void StartTimer()
    {
        // Start timer only if counter display is initialized
        if (_initialized)
        {
            ResetValues();
            _timerStarted = true;
        }
    }

    /// <summary>
    /// Triggered when player lets go of the wheel or when previous rotation session ended. 
    /// </summary>
    private void StopTimer()
    {
        // Stop timer only if counter display is initialized
        if (_initialized)
        {
            // Don't reset values here so that display shows the end result. Values are reset on the next rotation session.
            _timerStarted = false;
        }
    }

    /// <summary>
    /// Reset values for rotation counter
    /// </summary>
    private void ResetValues()
    {
        _timer = _initialTimerValue;
        _counter = 0;
    }
}
