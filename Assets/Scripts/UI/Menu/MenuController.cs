﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour 
{
    public static MenuController Instance = null;

    [SerializeField] private GameObject MainMenuPanel;
    [SerializeField] private GameObject OptionsPanel;
    [SerializeField] private GameObject SettingsButton;
    [SerializeField] private GameObject MainMenuButton;
    [SerializeField] private GameObject WinPanel;
    [SerializeField] private AudioClip ButtonClick;
    [SerializeField] private AudioClip WinClip;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject);
    }

    private void Start()
    {                    
        // Main menu scene
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            MainMenuButton.SetActive(false);
        }
        // Game scene
        else if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            MainMenuButton.SetActive(true);
        }      

        OptionsPanel.SetActive(false);
        if (WinPanel != null)
            WinPanel.SetActive(false);
    }

    public void ShowGameWonScreen()
    {
        SoundManager.Instance.PlaySoundClip(WinClip);
        WinPanel.SetActive(true);
    }

    public void Play()
    {
        SoundManager.Instance.PlaySoundClip(ButtonClick);
        SceneManager.LoadScene(1);
    }

    public void Options()
    {
        SoundManager.Instance.PlaySoundClip(ButtonClick);
        OptionsPanel.SetActive(true);
    }

    public void Back()
    {
        SoundManager.Instance.PlaySoundClip(ButtonClick);
        OptionsPanel.SetActive(false);       
    }

    public void MainMenu()
    {
        SoundManager.Instance.PlaySoundClip(ButtonClick);
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
