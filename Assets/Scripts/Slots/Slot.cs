﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class the holds info about the slot
/// </summary>
public class Slot : MonoBehaviour 
{
    #region PUBLIC FIELDS
    public SlotName SlotName;
    #endregion

    #region PRIVATE FIELDS
    [SerializeField] private BoxCollider Collider;
    #endregion

    #region PROPERTIES
    public GemInfo GemInfo { get; private set; }
    public bool IsEmpty { get; private set; }
    #endregion

    #region START
    private void Start()
    {
        InitializeValues();
    }
    #endregion

    #region PUBLIC METHODS
    /// <summary>
    /// Addjust slot values when gem is created
    /// </summary>
    /// <param name="gem">Instantiated gem</param>
    public void OnGemCreated(Gem gem)
    {
        OnGemDrop(gem.GemInfo);
        gem.CurrentSlot = SlotName;
    }

    /// <summary>
    /// Addjust slot values when gem is picked up
    /// </summary>
    /// <param name="gemInfo">Gem information</param>
    public void OnGemPickup(GemInfo gemInfo)
    {
        IsEmpty = true;
        GemInfo = null;
        Collider.enabled = true;
    }

    /// <summary>
    /// Addjust slot values when gem is dropped
    /// </summary>
    /// <param name="gemInfo">Gem information</param>
    public void OnGemDrop(GemInfo gemInfo)
    {
        IsEmpty = false;
        GemInfo = gemInfo;
        Collider.enabled = false;
    }
    #endregion

    #region PRIVATE METHODS
    private void InitializeValues()
    {
        Collider = GetComponentInChildren<BoxCollider>();
        IsEmpty = true;
        GemInfo = null;
    }
    #endregion
}
