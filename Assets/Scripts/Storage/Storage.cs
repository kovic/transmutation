﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Class that holds info about storage slot
/// </summary>
public class Storage : MonoBehaviour
{
    public GemType Type;

    // Reference transform for dropped gems
    [SerializeField ]private Transform _storageTransform;
    [SerializeField] private Text CounterText;   
    private List<Transform> StorageGemList = new List<Transform>();

    public Transform StorageTransform { get; private set; }    
    public bool IsEmpty { get; private set; }

    private void Start()
    {
        InitializeValues();
    }

    /// <summary>
    /// Adjusts storage values when gem is dropped on it
    /// </summary>
    /// <param name="gemTransform">Gem transform</param>
    public void OnGemDrop(Transform gemTransform)
    {
        // Add gem transform to the list and adjust the text
        StorageGemList.Add(gemTransform);
        CounterText.text = StorageGemList.Count.ToString();
        // Set IsEmpty property to false when only when first gem is dropped
        if (StorageGemList.Count == 1)
        {
            IsEmpty = false;            
        }
    }

    /// <summary>
    /// Adjusts storage values when gem is picked up
    /// </summary>
    public void OnGemPickUp()
    {
        StorageGemList.RemoveAt(0);
        if (StorageGemList.Count > 0)
        {
            // If storage still has gems, activate the first gem's (on the list) collider and adjust text
            StorageGemList[0].GetComponent<Gem>().Collider.enabled = true;
            CounterText.text = StorageGemList.Count.ToString();
        }
        else
        {
            // If last gem was picked up, adjust text and IsEmpty property
            CounterText.text = StorageGemList.Count.ToString();
            IsEmpty = true;
        }
    }

    /// <summary>
    /// Initialize values
    /// </summary>
    private void InitializeValues()
    {
        IsEmpty = true;
        StorageTransform = _storageTransform;
    }
}


