﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable object that holds info about each gem type
/// </summary>
[CreateAssetMenu(fileName = "Gem Info", menuName = "Gems/Gem Info", order = 1)]
public class GemInfo : ScriptableObject
{
    public GemType Type;
    public Material Material;
    public List<SlotName> InstantiationSlots;
    public Condition[] ConditionsArray;

    // this is dynamic data but because only one gem can be instantiated at a time, it's not a problem if all instantiated gems share this data
    public int DeltaCountRotation { get; set; }
    public Condition CurrentCondition { get; private set;}

    /// <summary>
    /// Sets current condition to the first condition in the conditions array and resets delta counter
    /// </summary>
    public void InitializeGemCondition()
    {
        CurrentCondition = ConditionsArray[0];
        DeltaCountRotation = 0;
    }
    
    /// <summary>
    /// Move to next condition in the array
    /// </summary>
    /// <returns>If there is any more conditions in the list</returns>
    public bool MoveToNextCondition()
    {
        DeltaCountRotation = 0;
        // Find the index of the current condition in the conditions array
        int index = Array.FindIndex(ConditionsArray, condition => condition == CurrentCondition);
        // Move to next condition and return true, or if there are no more conditions, return false
        if (index < ConditionsArray.Length - 1)
        {
            index++;
            CurrentCondition = ConditionsArray[index];
            return true;
        }
        else
            return false;
    }
}

