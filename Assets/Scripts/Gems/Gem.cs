﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Class that holds gem information and handles gem events
/// </summary>
public class Gem : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    #region PRIVATE FIELDS
    [SerializeField] private AudioClip _pickUpClip;
    [SerializeField] private AudioClip _dropClip;
    private Transform _transform;    
    private float _gemPickUpOffset = 4.0f;
    private float _rayLength = 6f;
    #endregion

    #region PROPERTIES
    public GemInfo GemInfo { get; set; }
    public SlotName CurrentSlot { get; set; }
    public Storage CurrentStorage { get; set; }
    public SphereCollider Collider { get; private set; }
    #endregion

    #region EVENTS
    /// <summary>
    /// Notifies when gem is picked up or dropped on the slot
    /// </summary>
    /// <param name="currentSlot">Pick up or drop slot</param>
    /// <param name="gemInfo">Gem information</param>
    public delegate void GemSlotHandlingDelegate(SlotName currentSlot, GemInfo gemInfo);
    public event GemSlotHandlingDelegate GemPickUpEvent;    
    public event GemSlotHandlingDelegate GemDropEvent;

    /// <summary>
    /// Notifies when gem is picked up from storage
    /// </summary>
    /// <param name="storage">Storage from which gem is picked up</param>
    public delegate void GemStoragePickUpDelegate(Storage storage);
    public event GemStoragePickUpDelegate GemStoragePickUpEvent;

    /// <summary>
    /// Notifies when gem is dropped on the storage
    /// </summary>
    /// <param name="transform">Gem's transform</param>
    /// <param name="storage">Storage on which gem is dropped</param>
    public delegate void GemStorageDropDelegate(Transform transform, Storage storage);
    public event GemStorageDropDelegate GemStorageDropEvent;
    #endregion

    #region START
    private void Start()
    {
        InitializeValues();        
    }
    #endregion

    #region PUBLIC METHODS
    /// <summary>
    /// Pick up and drag gem on the screen
    /// </summary>
    public void DragGem()
    {
        Vector3 newPosition = Input.mousePosition;
        newPosition.z = _gemPickUpOffset;
        Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(newPosition);
        _transform.position = mouseWorldPosition;
    }

    /// <summary>
    /// Drop gem to slot or storage
    /// </summary>
    public void GemDrop()
    {
        // Cast a ray from gem towards the ground
        Ray ray = new Ray(_transform.position, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, _rayLength))
        {
            // Cache hit collider's transform and its parent transform
            Transform collider = hit.collider.transform;
            Transform colliderParent = collider.parent;
            // If gem is over slot and slot is empty, put it on the slot
            if (hit.collider.CompareTag("Slot") && colliderParent.GetComponent<Slot>().IsEmpty)
            {
                CurrentSlot = colliderParent.GetComponent<Slot>().SlotName;
                CurrentStorage = null;                
                _transform.SetParent(colliderParent);
                _transform.position = collider.position;
            }
            // If gem is over the storage, or over the storaged gem, then put it on the storage
            else if ((hit.collider.CompareTag("Storage") || (hit.collider.CompareTag("Gem")) && collider.GetComponent<Gem>().CurrentSlot == SlotName.None) && colliderParent.GetComponent<Storage>().Type == GemInfo.Type)
            {                
                CurrentSlot = SlotName.None;
                CurrentStorage = colliderParent.GetComponent<Storage>();
                _transform.SetParent(colliderParent);
                _transform.position = CurrentStorage.StorageTransform.position;
                if (!CurrentStorage.IsEmpty)
                    Collider.enabled = false;
            }   
            // If gem is hitting anything else, just put it back to its original position
            else
            {
                _transform.position = _transform.parent.position;
            }
        }
        // If ray not hitting anything, just put it back to its original position
        else
        {
            _transform.position = _transform.parent.position;
        }

        // Handle events based on where the gem is dropped
        if (CurrentStorage != null)
        {
            GemStorageDropEvent(_transform, CurrentStorage);
        }
        else
        {
            GemDropEvent(CurrentSlot, GemInfo);
        }

        SoundManager.Instance.PlaySoundClip(_dropClip);
    }

    /// <summary>
    /// Handle gem pick up
    /// </summary>
    /// <param name="pointerEventData"></param>
    public void OnPointerDown(PointerEventData pointerEventData)
    {
        // Handle events based on where the gem is dropped
        if (CurrentStorage != null)
            GemStoragePickUpEvent(CurrentStorage);
        else
            GemPickUpEvent(CurrentSlot, GemInfo);
        // Pick up gem
        DragGem();
        SoundManager.Instance.PlaySoundClip(_pickUpClip);
    }

    /// <summary>
    /// Handle gem draging through the scene
    /// </summary>
    /// <param name="pointerEventData"></param>
    public void OnDrag(PointerEventData pointerEventData)
    {
        DragGem();
    }
    
    /// <summary>
    /// Handle gem dropping when mouse pointer is released
    /// </summary>
    /// <param name="pointerEventData"></param>
    public void OnPointerUp(PointerEventData pointerEventData)
    {
        GemDrop();
    }
    #endregion

    #region PRIVATE METHODS
    /// <summary>
    /// Initialize values
    /// </summary>
    private void InitializeValues()
    {
        _transform = transform;
        Collider = GetComponent<SphereCollider>();
    }
    #endregion
}
